# README #
Project 0: The <joe> Button

### What is this repository for? ###
This repository contains the prototype implementation of <joe> button design.
For more detail on the design and walkthrough of the <joe> button, please visit [my web portfolio](http://pages.cpsc.ucalgary.ca/~xhsun/work/joe.html)

### How do I get set up? ###

####To get the source code####
* You can clone or download the repository to access it from your local machine
* link for clone: git clone https://xhsun@bitbucket.org/xhsun/thejoebutton.git
* link for download: [link](https://bitbucket.org/xhsun/thejoebutton/get/cbd9b5d28129.zip)
* note: if you decide to download the repository, the link for download will provide you with a zip that contain the source code

####To Run The Program####
* Open your visual studio and from there open the joe_Button project. Once its loaded, you can just click on the run button to run the program.
* For those who don't have visual studio, all you need to do is get the launcher.exe and double click on it!

### Contribution guidelines ###

* Author: Hannah Sun

### Who do I talk to? ###

* Hannah Sun: xhsun@ucalgary.ca
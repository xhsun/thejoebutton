﻿using joe_Button.misc;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
namespace joe_Button
{
    /// <summary>
    /// The joe button main window
    /// Author: Hannah Sun
    /// Last Modified: 2015-09-24
    /// </summary>
    public partial class MainWindow : Window
    {
        //inits
        public int orbNum = 0;
        private constants constant;
        private int foodCount = 0;
        private int orbCount = 0;
        private Storyboard eatFood;
        private Storyboard wakeup;
        private Storyboard showHeart;
        private Storyboard foodDroup;

        public MainWindow()
        {
            InitializeComponent();
            constant = constants.getInstance();
            constant.main = this;
            initStory();
        }

        /// <summary>
        /// initialize all story board that will be used in multiple location
        /// </summary>
        private void initStory()
        {
            foodDroup = (Storyboard)FindResource("food_drop");
            foodDroup.Completed += (s, e1) =>
            {
                bowl.Source = new BitmapImage(new Uri(@"..\image\dog_bowl_full.png", UriKind.Relative));
            };

            eatFood = (Storyboard)FindResource("eat");
            eatFood.Completed += (s1, e1) =>
            {   //enable button click after animation ended
                food_source.IsEnabled = true;
            };

            //after wakeup complete do the animation for eating
            wakeup = (Storyboard)FindResource("wake");
            wakeup.Completed += (s, e) =>
            {
                eatFood.Begin(this);
            };

            //init the animation of floating heart
            //after the animation complete, make the orb
            //correspond to the heart visisble
            showHeart = (Storyboard)FindResource("spawn");
            showHeart.Completed += (s2, e2) =>
            {
                switch (orbCount)
                {
                    case 0:
                        family_orb.Visibility = Visibility.Visible;
                        break;
                    case 1:
                        friend_orb.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        game_orb.Visibility = Visibility.Visible;
                        break;
                    case 3:
                        animal_orb.Visibility = Visibility.Visible;
                        break;
                    case 4:
                        pokemon_orb.Visibility = Visibility.Visible;
                        break;
                    default:
                        orbCount = 0;
                        return;
                }
                orbCount++;
            };
        }

        /// <summary>
        /// When the food source is click drop down the food according
        /// to the food count.
        ///     -then do the eating animation if a food is dropped
        ///     -else do the animation for clear canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sourceClicked(object sender, RoutedEventArgs e)
        {
            switch (foodCount)
            {
                case 0:
                    food.Source = new BitmapImage(new Uri(@"..\image\burger.png", UriKind.Relative));
                    break;
                case 1:
                    food.Source = new BitmapImage(new Uri(@"..\image\chicken.png", UriKind.Relative));
                    break;
                case 2:
                    food.Source = new BitmapImage(new Uri(@"..\image\fries.png", UriKind.Relative));
                    break;
                case 3:
                    food.Source = new BitmapImage(new Uri(@"..\image\soda.png", UriKind.Relative));
                    break;
                case 4:
                    food.Source = new BitmapImage(new Uri(@"..\image\raspberry.png", UriKind.Relative));
                    break;
                case 5: //not food clicked, drop not food and do animation for clear canvas
                    food.Source = new BitmapImage(new Uri(@"..\image\notFood.png", UriKind.Relative));
                    foodCount = 0;
                    notFood();
                    return;
                default:
                    foodCount = 0;
                    return;
            }
            //increment food count and do food drop animation
            foodCount++;
            foodDroup.Begin(this);
            //disable food soruce for now
            food_source.IsEnabled = false;
            //make the dog eat
            var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2) };
            timer.Start();
            timer.Tick += (s1, e2) =>
            {
                timer.Stop();
                dogEat();
            };
            
        }

        /// <summary>
        /// do a series of action for dog eating the food
        /// if the dog is sleeping, do the wakeup animation
        /// </summary>
        private async void dogEat()
        {
            if (foodCount == 1)
            { //first food, need wake up
                sleep.Visibility = Visibility.Hidden;
                //display dialog
                await Task.Delay(1000);
                expression.Content = "?";
                await Task.Delay(1500);
                expression.Content = "!";
                await Task.Delay(2000);
                //clear dialog
                expression.Content = "";
                //wakeup animation
                wakeup.Begin(this);
                await Task.Delay(1300);
                //change food bowl from full to empty
                bowl.Source = new BitmapImage(new Uri(@"..\image\dog_bowl.png", UriKind.Relative));
                await Task.Delay(1800);
                //display a heart
                showHeart.Begin(this);
                return;
            }
            //display a heart then do eat food animation
            showHeart.Begin(this);
            await Task.Delay(1700);
            eatFood.Begin(this);
            await Task.Delay(1000);
            //change food bowl from full to empty
            bowl.Source = new BitmapImage(new Uri(@"..\image\dog_bowl.png", UriKind.Relative));
        }

        /// <summary>
        /// the animation sequence for clear canvas
        /// </summary>
        private async void notFood()
        {
            //drop not food
            foodDroup.Begin(this);
            //disable food soruce for now
            food_source.IsEnabled = false;
            await Task.Delay(1000);
            //reset dialog position and start dialog
            Canvas.SetTop(expression, 266);
            Canvas.SetLeft(expression, 215);
            expression.Content = "...";
            await Task.Delay(600);
            ((Storyboard)FindResource("changeToSad")).Begin(this);
            await Task.Delay(300);
            expression.Content = "Onion...";
            await Task.Delay(600);
            expression.Content = "...";
            await Task.Delay(600);
            expression.Content = "Really...";
            //change heart image to break heart
            dog_heart.Source = new BitmapImage(new Uri(@"..\image\breakHeart.png", UriKind.Relative));
            await Task.Delay(300);
            //clear dialog and init heart animation
            expression.Content = "";
            showHeart.Begin(this);
            await Task.Delay(600);
            //do sleep animation and display zzz
            ((Storyboard)FindResource("sleep")).Begin(this);
            await Task.Delay(300);
            sleep.Visibility = Visibility.Visible;
            await Task.Delay(500);
            //display animation of orbs running away
            ((Storyboard)FindResource("orb_run_1")).Begin(this);
            ((Storyboard)FindResource("orb_run_2")).Begin(this);
            ((Storyboard)FindResource("orb_run_3")).Begin(this);
            ((Storyboard)FindResource("orb_run_4")).Begin(this);
            ((Storyboard)FindResource("orb_run_5")).Begin(this);
            await Task.Delay(300);
            //manually hide orbs because using animation to hide them
            //will make them unable to display
            game_orb.Visibility = Visibility.Hidden;
            await Task.Delay(100);
            family_orb.Visibility = Visibility.Hidden;
            await Task.Delay(100);
            friend_orb.Visibility = Visibility.Hidden;
            animal_orb.Visibility = Visibility.Hidden;
            await Task.Delay(300);
            pokemon_orb.Visibility = Visibility.Hidden;
            await Task.Delay(4200);
            //display animation for not food running away
            Canvas.SetTop(food, 381);
            ((Storyboard)FindResource("onion_run")).Begin(this);
            await Task.Delay(1300);
            //change food bowl back to empty
            bowl.Source = new BitmapImage(new Uri(@"..\image\dog_bowl.png", UriKind.Relative));
            //reset states
            reset();
        }

        /// <summary>
        /// reset nesscarry states for next round of animation
        /// </summary>
        private void reset()
        {
            orbNum = 0;
            foodCount = 0;
            orbCount = 0;
            food_source.IsEnabled = true;
            Canvas.SetTop(expression, 350);
            Canvas.SetLeft(expression, 264);
            Canvas.SetTop(food, 178);
            Canvas.SetLeft(food, 340);
            dog_heart.Source = new BitmapImage(new Uri(@"..\image\heart.png", UriKind.Relative));
        }

        /// <summary>
        /// depend on which orb been pressed,
        /// display the content of the orb (each orb have different content)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void orbClicked(object sender, RoutedEventArgs e)
        {
            string name = ((Button)sender).Name;
            switch (name)
            {
                case "family_orb":
                    orbNum = 1;
                    break;
                case "friend_orb":
                    orbNum = 2;
                    break;
                case "game_orb":
                    orbNum = 3;
                    break;
                case "animal_orb":
                    orbNum = 4;
                    break;
                case "pokemon_orb":
                    orbNum = 5;
                    break;
                default:
                    orbNum = 0;
                    break;
            }
            //init an orb popup to display content
            orb thisOrb = new orb();
            this.popups.Children.Add(thisOrb);
        }

        /// <summary>
        /// close orb popup when user click on the grey area
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closePopup(object sender, MouseButtonEventArgs e)
        {
            //hide the mask and "close" the popup
            this.popups.Height = 300;
            this.popups.Width = 300;
            this.popupBack.Visibility = Visibility.Collapsed;
            this.popups.Children.Clear();
        }
    }
}

﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace joe_Button.misc
{
    /// <summary>
    /// display the content of the selected orb
    /// Author: Hannah Sun
    /// Last Modified: 2015-09-23
    /// </summary>
    public partial class orb : UserControl
    {
        private constants constant;
        public orb()
        {
            InitializeComponent();
            constant = constants.getInstance();
            //reset panel
            constant.main.popups.Children.Clear();
            constant.main.popupBack.Visibility = Visibility.Visible;
            //use the orb number to decied which img would be apporate to display
            switch (constant.main.orbNum)
            {
                case 1:
                    orb_content.Source = new BitmapImage(new Uri(@"..\image\family.png", UriKind.Relative));
                    break;
                case 2:
                    orb_content.Source = new BitmapImage(new Uri(@"..\image\friend.png", UriKind.Relative));
                    break;
                case 3:
                    orb_content.Source = new BitmapImage(new Uri(@"..\image\game.png", UriKind.Relative));
                    break;
                case 4:
                    orb_content.Source = new BitmapImage(new Uri(@"..\image\animal.png", UriKind.Relative));
                    break;
                case 5:
                    orb_content.Source = new BitmapImage(new Uri(@"..\image\pokemon.png", UriKind.Relative));
                    break;
                default: //close popup, nothing to show here
                    constant.main.popups.Children.Clear();
                    constant.main.popupBack.Visibility = Visibility.Collapsed;
                    break;
            }
        }

        /// <summary>
        /// close the orb popup when user click on image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void orbClose(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            constant.main.popups.Children.Clear();
            constant.main.popupBack.Visibility = Visibility.Collapsed;
        }
    }
}

﻿namespace joe_Button.misc
{
    /// <summary>
    /// a class to contain constants that will be shared between
    /// multiple classes.
    /// Author: Hannah Sun
    /// Last Modified: 2015-09-23
    /// </summary>
    class constants
    {
        private static constants constant;
        public MainWindow main { get; set; }

        /// <summary>
        /// get instance of this class
        /// since this class is singleton, constructor will not be available
        /// </summary>
        /// <returns>instance of this class</returns>
        public static constants getInstance()
        {
            if (constant == null) { constant = new constants(); }
            return constant;
        }
    }
}
